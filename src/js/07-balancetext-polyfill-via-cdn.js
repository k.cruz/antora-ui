;(function () {
  const polyfillScript = document.createElement('script')
  polyfillScript.src = 'https://cdn.jsdelivr.net/npm/balance-text@3.3.0/balancetext.min.js'

  document.head.append(polyfillScript)

  polyfillScript.onload = function () {
    // balanceText will be defined once the polyfill loads, but of course isn't defined yet
    balanceText() // eslint-disable-line no-undef
  }
})()
